#!/bin/sh

mkdir -p "$HOME/.config"

# Av portabilitetsskäl
echon () { printf '%s' "$1"; }
echoe () { printf '%s\n' "$1" 1>&2; }

# QR-kod för snabbare handläggning enligt "UsingQR"-specifikationen.
#
# https://www.qrkod.info/
# (alt. https://web.archive.org/web/20230317190509/https://www.qrkod.info/)
#
# https://www.qrkod.info/specification.pdf
# (alt. https://web.archive.org/web/20230317190519/https://www.qrkod.info/specification.pdf)

qrsfld () { echon "\"$1\":\"$2\""; }
qrifld () { echon "\"$1\": $2"; }

make_qr () {
	out="$1"; acc="$2"; sum="$3"; mnm="$4"; msg="$5"

	F1="$(qrifld uqr  "1")"
	F2="$(qrifld tp   "1")"
	F3="$(qrsfld cid  "822002-8040")"
	F4="$(qrsfld nme  "(medlem) $mnm")"
	F5="$(qrsfld pt   "BBAN")"
	F6="$(qrsfld acc  "$acc")"
	F7="$(qrsfld due  "$sum")"
	F8="$(qrsfld iref "$msg")"

	qrencode -t EPS -m 0 -o "$out" "{$F1, $F2, $F3, $F4, $F5, $F6, $F7, $F8}"
}

# Läser ett datum, kräver ISO-8601
read_date() {
	while :; do
		echoe "Ange datum på kvittot (YYYY-MM-DD):";
		IFS= read -r date;

		echon "$date" | grep '^[0-9]\{4\}-[0-9]\{2\}-[0-9]\{2\}$' > /dev/null;
		if [ $? -eq 0 ]; then
			echon "$date"; echoe; break;
		fi

		echoe "Felaktigt datumformat."; echoe;
	done
}

# Beskrivningslängden begränsas av papperstorleken
read_desc() {
	while :; do
		echoe "Ange beskrivning (max 40 tecken):";
		IFS= read -r desc;

		# Tolerera vissa TeX-tecken
		# Hanteras senare
		desc="$(echon "$desc" | sed 's![^[:alpha:][:digit:]#$%&~_^ ,.;/-]!!g')"

		if [ "$(echon "$desc" | wc -m)" -le 40 ]; then
			echon "$desc"; echoe; break;
		fi

		echoe "För lång beskrivning."; echoe;
	done
}

# Beloppstorleken begränsas av rimlighetsskäl
read_tsum() {
	while :; do
		echoe "Ange belopp (SEK):";
		echoe "    (Avrunda uppåt till hela kronor.)";
		IFS= read -r tsum;

		echon "$tsum" | grep '^[0-9]\{1,6\}$' > /dev/null;

		if [ $? -eq 0 ]; then
			echon "$tsum"; echoe; break;
		fi

		echoe "Felaktigt belopp."; echoe;
	done
}

# Namnlängd begränsas av Nordeas mobilapp.
read_name() {
	while :; do
		echoe "Ange för- & efternamn (max 35 tecken):";
		IFS= read -r name;
		name="$(echon "$name" | sed 's/[^[:alpha:] -]//g')"

		if [ "$(echon "$name" | wc -m)" -le 35 ]; then
			echon "$name"; echoe; break;
		fi

		echoe "För långt namn, vänligen förkorta."; echoe;
	done
}

# Kontonumrets längd begränsas av pappersstorleken.
# Gränsen nås aldrig av giltiga kontonummer i rimligt format.
read_accn() {
	echoe "Ange clearingnummer:"
	IFS= read -r acnc;
	echoe "Ange kontonummer:"
	IFS= read -r acnk;
	# Tillåt vissa skiljetecken, dessa saknar betydelse
	accn="$(echon "$acnc $acnk" | sed 's/[^[:digit:] ,.-]//g' | cut -c 1-40 )";
	echon "$accn"; echoe;
}

# Banknanmnets längd begränsas av pappersstorleken.
# Gränsen nås aldrig av rimligt döpta banker.
read_bank() {
	echoe "Ange bank:";
	IFS= read -r bank;
	bank="$(echon "$bank" | sed 's/[^[:alpha:][:digit:] -]//g' | cut -c 1-40)"
	echon "$bank"; echoe;
}


# Primitiv flagghantering
if   [ "$1" = '-p' ]; then
	pass='0';
elif [ -n "$1" ]; then
	#Hjälp, samt ogiltiga flaggor
	echo ""
	echo "Interaktivt program för att kompilera en kvittomall till Lysators"
	echo "kassör. I mallen ingår ordinarie uppgifter samt en QR-kod för"
	echo "snabbare handläggning."
	echo ""
	echo "Användning: kvittomall [-p|-h]"
	echo "    -p: Kompilera mall utan uppgifter."
	echo "    -h: Visa denna information."
	exit 0;
else
	pass='1';
fi

# Slumpa ett transaktions-ID
# Basera ett tillfälligt mappnamn på detta
# uuencode tillser att tr endast matas med giltiga tecken i den kodning som
# miljön efterfrågar. fold -w 8 begränsar längden på strängen eftersom head -c
# inte är portabel

tuid=$(cat /dev/urandom | od -A n -vx | tr -dc '0-9' | fold -w 8 | head -n 1)
tuid="LYS-$tuid"

echo;
echo "Kvitto-ID: $tuid"

# Kör i $HOME om möjligt, annars /tmp
wdir=$( [ -z "$HOME" ] && echon "/tmp" || echon "$HOME" );
tmp_path="$wdir/.kvittomall_$tuid"

mkdir "$tmp_path" &&
cat "$2" > "$tmp_path/base.tex" &&
cat "$3" > "$tmp_path/logo.pdf" &&
cd "$tmp_path" &&
printf '' > kvittomall.tex || exit 1;

echo;

if [ "$pass" -eq '1' ]; then

	date="$(read_date)";
	desc="$(read_desc)";
	tsum="$(read_tsum)";

	{ # Försök läsa namn, kontonummer och bank från configfilen
		IFS= read -r name 2> /dev/null &&\
		IFS= read -r accn 2> /dev/null &&\
		IFS= read -r bank 2> /dev/null;
	} < "$HOME/.config/lysator-kvittomall" 2> /dev/null

	# Läs namn från aktuellt konto
	if [ -z "$name" ]; then
		name=$(getent passwd `whoami` | cut -d : -f 5 | sed 's/[^[:alpha:] -]//g'| cut -c 1-35)
	fi
	if [ -z "$accn" ]; then
		accn="$(read_accn)";
	fi
	if [ -z "$bank" ]; then
		bank="$(read_bank)";
	fi


	while :; do

		echo ""
		echo "Verifiera."
		echo ""
		echo "[1] Datum på kvittot: $date"
		echo "[2] Beskrivning: $desc"
		echo "[3] Belopp (SEK): $tsum"
		echo ""
		echo "[4] Namn: $name"
		echo "[5] Kontonummer (ink. clearing): $accn"
		echo "[6] Bank: $bank"
		echo ""
		echo "Fortsätt med <enter>, ändra fält med <siffra><enter>."
		echon ":";

		IFS= read -r corr;

		if [ -z "$corr" ]; then
			break;
		fi;
		echo;

		if   [ "$corr" = "1" ]; then
			date="$(read_date)";
		elif [ "$corr" = "2" ]; then
			desc="$(read_desc)";
		elif [ "$corr" = "3" ]; then
			tsum="$(read_tsum)";
		elif [ "$corr" = "4" ]; then
			name="$(read_name)";
		elif [ "$corr" = "5" ]; then
			accn="$(read_accn)";
		elif [ "$corr" = "6" ]; then
			bank="$(read_bank)";
		fi;
	done

	# Spara ev. nya värden på namn, kononummer och bank
	printf '%s\n%s\n%s\n' "$name" "$accn" "$bank" > "$HOME/.config/lysator-kvittomall"

	# Reducera kontonumret till siffror, alla skiljetecken saknar betydelse
	accn=$(echon "$accn" | sed 's/[^[:digit:]]//g');
	# TeX-säkra beskrivningen, som är det enda sanna fritextfältet
	desc="$(echon "$desc" | sed 's/\([#$%&~_^]\)/\\\1{}/g')"

	# Skapa QR-kod
	make_qr "qr.eps" "$accn" "$tsum" "$name" "$tuid"

	# Tryck variabler till TeX-mallen
	printf '\\def\\VDATE{%s}\n' "$date" >> kvittomall.tex
	printf '\\def\\VDESC{%s}\n' "$desc" >> kvittomall.tex
	printf '\\def\\VNAME{%s}\n' "$name" >> kvittomall.tex
	printf '\\def\\VTSUM{%s}\n' "$tsum" >> kvittomall.tex
	printf '\\def\\VFLDA{%s}\n' "$bank" >> kvittomall.tex
	printf '\\def\\VFLDB{%s}\n' "$accn" >> kvittomall.tex
	printf '\\def\\VFLDC{%s}\n' "$tuid" >> kvittomall.tex

	printf '\\def\\BCHKA{%s}\n' "isdef" >> kvittomall.tex
	printf '\\def\\USEQR{%s}\n' "isdef" >> kvittomall.tex
fi;

cat "base.tex" >> kvittomall.tex
rm "base.tex"

echo "Trycker mall..."

# Sannolikt den sårbaraste delen av skriptet, kräver ordentlig TeX-miljö

latexmk -lualatex;


first="";

while :; do
	echo ""
	echo "Öppna, skriv ut, eller avsluta?"
	echo "Öppna:    O [kommando =evince]"
	echo "Skriv ut: P [kommando =lpr]"
	echo "Spara:    S [abs. sökväg =${wdir}/kvittomall-${tuid}.pdf]"
	echo "Avsluta:  Q";
	echon ":";
	IFS= read -r istr;
	icmd="$(echon "$istr  " | cut -c 1)"
	istr="$(echon "$istr  " | cut -c 2- | sed 's/^[[:space:]]*//')";
	if [ -z "$istr" ]; then
		ocmd='evince';
		pcmd='lpr';
		spth="${wdir}/kvittomall-${tuid}.pdf";
	else
		ocmd="$istr";
		pcmd="$istr";
		spth="$istr";
	fi

	if   [ "$icmd" = 'O' ] || [ "$icmd" = 'o' ]; then
		echo "Öppnar."; echo;
		eval "$ocmd kvittomall.pdf" &
	elif [ "$icmd" = 'P' ] || [ "$icmd" = 'p' ]; then
		echo "Skriver ut."; echo;
		cat "kvittomall.pdf" | eval "$pcmd";
	elif [ "$icmd" = 'S' ] || [ "$icmd" = 's' ]; then
		echo "Sparar som $spth."; echo;
		cat "kvittomall.pdf" > "$spth"
	elif [ "$icmd" = 'Q' ] || [ "$icmd" = 'q' ]; then
		if [ -z "$first" ]; then
			echo ""
			echo "Kvittomall ej öppnad/utskriven/sparad.";
			echo "Bekräfta:";
		else
			echo "Avslutar."; echo;
			break;
		fi
	fi
	first="-";
done

cd && rm -r "$tmp_path" || echoe "Could not remove working directory.";
