Kompilerar kvittomall som PDF med godtyckliga uppgifter.

Lagrar namn, kontonummer och bank i configfil.

Körs med `./kvitto.sh -h` för första användning.

Licensieras enligt EUPL.
